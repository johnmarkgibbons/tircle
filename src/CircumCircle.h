#pragma once
#include <ImathVec.h>

Imath::V3d circumcircle(Imath::V3d &pointA, Imath::V3d &pointB, Imath::V3d &pointC);