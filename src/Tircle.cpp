#include "Tircle.h"
#include "CircumCircle.h"
#include "Trilaterate.h"
#include <iostream>
#include <fstream>
#include <cmath>
#include <algorithm>
#include <iterator>

using namespace Imath;
using namespace std;


template <typename T1, typename T2>
void sortByPerm(vector<T1> list1, vector<T2>& list2) {
	const auto len = list1.size();
	if (!len || len != list2.size()) throw;

	// create permutation vector
	vector<size_t> perms;
	for (size_t i = 0; i < len; i++) perms.push_back(i);
	sort(perms.begin(), perms.end(), [&](T1 a, T1 b) { return list1[a] < list1[b]; });

	// order input vectors by permutation
	for (size_t i = 0; i < len - 1; i++) {
		swap(list1[i], list1[perms[i]]);
		swap(list2[i], list2[perms[i]]);

		// adjust permutation vector if required
		if (i < perms[i]) {
			auto d = distance(perms.begin(), find(perms.begin() + i, perms.end(), i));
			swap(perms[i], perms[d]);
		}
	}
}


Tircle::Tircle()
{
}


Tircle::~Tircle()
{
}

void Tircle::addPoints(vector<double> pnts)
{
	for (unsigned i = 0; i < pnts.size() / 3; i++)
	{
		points.push_back(V3d(pnts[3 * i], pnts[3 * i + 1], pnts[3 * i + 2]));
	}
}

void Tircle::addNormals(vector<double> nrmls)
{
	for (unsigned i = 0; i < nrmls.size() / 3; i++)
	{
		normals.push_back(V3d(nrmls[3 * i], nrmls[3 * i + 1], nrmls[3 * i + 2]));
	}
}

void Tircle::addTriangles(vector<int> tris)
{
	for (unsigned i = 0; i < tris.size() / 3; i++)
	{
		int tri[3]{ tris[3 * i], tris[3 * i + 1], tris[3 * i + 2] };
		triangles.insert(triangles.end(), tri, tri + 3);
	}
}


void Tircle::makeCircles()
{
	vertexGroups.clear();
	for (unsigned i = 0; i != triangles.size() / 3; i++)
	{
		V3d c = circumcircle(points[triangles[3 * i]], points[triangles[3 * i + 1]], points[triangles[3 * i + 2]]);
		centers.push_back(c.x);
		centers.push_back(c.y);
		centers.push_back(c.z);
		//cout << (c - points[triangles[3 * i]]).length() << endl;
		radii.push_back((c - points[triangles[3 * i]]).length());
		int vg[3]{ i, i, i };
		vertexGroups.insert(vertexGroups.end(), vg, vg + 3);

	}
	sortByPerm(triangles, vertexGroups);
	// Reorder to imply normal direction
	for (unsigned i = 0; i < vertexGroups.size() / 3; i++)
	{
		V3d centerA = { centers[3 * vertexGroups[3 * i + 0]], centers[3 * vertexGroups[3 * i + 0] + 1], centers[3 * vertexGroups[3 * i + 0] + 2] };
		V3d centerB = { centers[3 * vertexGroups[3 * i + 1]], centers[3 * vertexGroups[3 * i + 1] + 1], centers[3 * vertexGroups[3 * i + 1] + 2] };
		V3d centerC = { centers[3 * vertexGroups[3 * i + 2]], centers[3 * vertexGroups[3 * i + 2] + 1], centers[3 * vertexGroups[3 * i + 2] + 2] };
		V3d normal = (centerA - centerB).cross(centerB - centerC);
		if (normal.dot(normals[i]) < 0.0)
		{
			swap(vertexGroups[3 * i + 0], vertexGroups[3 * i + 2]);
		}
	}
}




void Tircle::makeTriangles()
{
	points.clear();
	for (unsigned i = 0; i < vertexGroups.size() / 3; i++)
	{
		V3d centerA = { centers[3 * vertexGroups[3 * i + 0]], centers[3 * vertexGroups[3 * i + 0] + 1], centers[3 * vertexGroups[3 * i + 0] + 2] };
		V3d centerB = { centers[3 * vertexGroups[3 * i + 1]], centers[3 * vertexGroups[3 * i + 1] + 1], centers[3 * vertexGroups[3 * i + 1] + 2] };
		V3d centerC = { centers[3 * vertexGroups[3 * i + 2]], centers[3 * vertexGroups[3 * i + 2] + 1], centers[3 * vertexGroups[3 * i + 2] + 2] };	
		V3d resultA, resultB;
		trilaterate(&resultA, &resultB, centerA, radii[vertexGroups[3 * i + 0]], centerB, radii[vertexGroups[3 * i + 1]], centerC, radii[vertexGroups[3 * i + 2]], 0.0);
		// Get result closest to implied normal
		//cout << resultA << "    " << resultB << endl;
		if ((resultA - resultB).dot((centerA - centerB).cross(centerB - centerC)) > 0.0)
			points.push_back(resultA);
		else
			points.push_back(resultB);
	}
}


void Tircle::print()
{
	//cout << "Centers" << endl;
	////for (int i = 0; i != Tircle::positions.size(); i++);
	//for (auto p = centers.begin(); p != centers.end(); p++)
	//{
	//	cout << *p << endl;
	//	//cout << Tircle::positions[i] << Tircle::orientations[i] << Tircle::radii[i] << endl;
	//}

	//cout << "Radii" << endl;
	////for (int i = 0; i != Tircle::positions.size(); i++);
	//for (auto r = radii.begin(); r != radii.end(); r++)
	//{
	//	cout << *r << endl;
	//	//cout << Tircle::positions[i] << Tircle::orientations[i] << Tircle::radii[i] << endl;
	//}

	cout << "Positions" << endl;
	//for (int i = 0; i != Tircle::positions.size(); i++);
	for (auto p = points.begin(); p != points.end(); p++)
	{
		cout << *p << endl;
		//cout << Tircle::positions[i] << Tircle::orientations[i] << Tircle::radii[i] << endl;
	}

	//cout << "Normals" << endl;
	////for (int i = 0; i != Tircle::positions.size(); i++);
	//for (auto n = normals.begin(); n != normals.end(); n++)
	//{
	//	cout << *n << endl;
	//	//cout << Tircle::positions[i] << Tircle::orientations[i] << Tircle::radii[i] << endl;
	//}
	//cout << "VTXGRPS" << endl;
	////for (int i = 0; i != Tircle::positions.size(); i++);
	//for (auto vg = vertexGroups.begin(); vg != vertexGroups.end(); vg++)
	//{
	//	cout << *vg << endl;
	//	//cout << Tircle::positions[i] << Tircle::orientations[i] << Tircle::radii[i] << endl;
	//}
}

void Tircle::save(string path)
{
	ofstream f;
	f.open(path);

	f << "Centers" << endl;
	for (auto p = centers.begin(); p != centers.end(); p++)
	{
		f << *p << " ";
	}
	f << endl;

	f << "Radii" << endl;
	for (auto r = radii.begin(); r != radii.end(); r++)
	{
		f << *r << " ";
	}
	f << endl;

	f << "VertexGroups" << endl;
	for (auto vg = vertexGroups.begin(); vg != vertexGroups.end(); vg++)
	{
		f << *vg << " ";
	}
	f.close();
}