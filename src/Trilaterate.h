#pragma once
#include <ImathVec.h>

int trilaterate(Imath::V3d *const result1, Imath::V3d *const result2,
	const Imath::V3d p1, const double r1,
	const Imath::V3d p2, const double r2,
	const Imath::V3d p3, const double r3,
	const double maxzero);

