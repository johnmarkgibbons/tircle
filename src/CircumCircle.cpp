#include <algorithm>
#include <iterator>
#include <ImathVec.h>

using namespace Imath;
using namespace std;


V3d circumcircle(V3d &pointA, V3d &pointB, V3d &pointC)
{
	//cout << "Points" << endl;
	//cout << pointA << pointB << pointC << endl;
	// Get first perpendicular bisector
	V3d midVecA = (pointB - pointA)*0.5;

	//cout << "midVecA" << endl;
	//cout << midVecA << endl;

	V3d midPointA = pointA + midVecA;

	//cout << "midPointA" << endl;
	//cout << midPointA << endl;

	V3d upVec = pointA - pointC;
	//cout << "upVec" << endl;
	//cout << upVec << endl;

	V3d normal = midVecA.cross(upVec);
	//cout << "normal" << endl;
	//cout << normal << endl;


	V3d slopeVecA = midVecA.cross(normal);
	//cout << "slopeVecA" << endl;
	//cout << slopeVecA << endl;

	// Get second perpendicular bisector
	V3d midVecB = (pointC - pointB)*0.5;

	//cout << "midVecB" << endl;
	//cout << midVecB << endl;

	V3d midPointB = pointB + midVecB;

	//cout << "midPointB" << endl;
	//cout << midPointB << endl;

	V3d slopeVecB = midVecB.cross(normal);

	//cout << "slopeVecB" << endl;
	//cout << slopeVecB << endl;

	// Get the center of the circle from the intersection of the bisectors
	//cout << "Result" << endl;
	V3d res = midPointA + (midPointB - midPointA).cross(slopeVecB).length() / slopeVecA.cross(slopeVecB).length() * slopeVecA;
	//cout << res << endl;
	//cout << "___________________________________" << endl;
	return midPointA + (midPointB - midPointA).cross(slopeVecB).length() / slopeVecA.cross(slopeVecB).length() * slopeVecA;;
}