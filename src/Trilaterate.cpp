#include <stdio.h>
#include <math.h>
#include <ImathVec.h>
using namespace Imath;

/* No rights reserved (CC0, see http://wiki.creativecommons.org/CC0_FAQ).
* The author has waived all copyright and related or neighboring rights
* to this program, to the fullest extent possible under law.
*/

/* Largest nonnegative number still considered zero */
#define   MAXZERO  0.0

/* Return zero if successful, negative error otherwise.
* The last parameter is the largest nonnegative number considered zero;
* it is somewhat analoguous to machine epsilon (but inclusive).
*/
int trilaterate(V3d *const result1, V3d *const result2,
	const V3d p1, const double r1,
	const V3d p2, const double r2,
	const V3d p3, const double r3,
	const double maxzero)
{
	V3d	ex, ey, ez, t1, t2;
	double	h, i, j, x, y, z, t;

	/* h = |p2 - p1|, ex = (p2 - p1) / |p2 - p1| */
	ex = p2 - p1;
	h = ex.length();
	if (h <= maxzero) {
		/* p1 and p2 are concentric. */
		return -1;
	}
	ex = ex / h;

	/* t1 = p3 - p1, t2 = ex (ex . (p3 - p1)) */
	t1 = p3 - p1;
	i = ex.dot(t1);
	t2 = ex * i;
	/* ey = (t1 - t2), t = |t1 - t2| */
	ey = t1 - t2;
	t = ey.length();
	if (t > maxzero) {
		/* ey = (t1 - t2) / |t1 - t2| */
		ey = ey / t;

		/* j = ey . (p3 - p1) */
		j = ey.dot(t1);
	}
	else
		j = 0.0;

	/* Note: t <= maxzero implies j = 0.0. */
	if (fabs(j) <= maxzero) {
		/* p1, p2 and p3 are colinear. */

		/* Is point p1 + (r1 along the axis) the intersection? */
		t2 = p1 + ex * r1;
		if (fabs((p2 - t2).length() - r2) <= maxzero &&
			fabs((p3 - t2).length() - r3) <= maxzero) {
			/* Yes, t2 is the only intersection point. */
			if (result1)
				*result1 = t2;
			if (result2)
				*result2 = t2;
			return 0;
		}

		/* Is point p1 - (r1 along the axis) the intersection? */
		t2 = p1 + ex * -r1;
		if (fabs((p2 - t2).length() - r2) <= maxzero &&
			fabs((p3 - t2).length() - r3) <= maxzero) {
			/* Yes, t2 is the only intersection point. */
			if (result1)
				*result1 = t2;
			if (result2)
				*result2 = t2;
			return 0;
		}

		return -2;
	}

	/* ez = ex x ey */
	ez = ex.cross(ey);

	x = (r1*r1 - r2 * r2) / (2 * h) + h / 2;
	y = (r1*r1 - r3 * r3 + i * i) / (2 * j) + j / 2 - x * i / j;
	z = r1 * r1 - x * x - y * y;
	if (z < -maxzero) {
		/* The solution is invalid. */
		return -3;
	}
	else
		if (z > 0.0)
			z = sqrt(z);
		else
			z = 0.0;

	/* t2 = p1 + x ex + y ey */
	t2 = p1 + ex * x;
	t2 = t2 + ey * y;

	/* result1 = p1 + x ex + y ey + z ez */

	if (result1)
		*result1 = t2 + ez * z;

	/* result1 = p1 + x ex + y ey - z ez */
	if (result2)
		*result2 = t2 + ez * -z;

	return 0;
}