#pragma once
#include <ImathVec.h>
#include <vector>

class Tircle
{
public:
	Tircle();
	~Tircle();
	void addPoints(std::vector<double> points);
	void addNormals(std::vector<double> normals);
	void addTriangles(std::vector<int> triangles);
	void makeCircles();
	void save(std::string path);
	void makeTriangles();
	void print();
private:
	std::vector<Imath::V3d> points;
	std::vector<Imath::V3d> normals;
	std::vector<int> triangles;
	std::vector<double> centers;
	std::vector<double> radii;
	std::vector<double> vertexGroups;
};

